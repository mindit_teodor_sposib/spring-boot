package com.boot;

import com.boot.controller.HomeController;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by MINDIT-PC on 07/07/2017.
 */
public class AppTest {

    @Test
    public void testApp(){
        HomeController hc = new HomeController();
        String result = hc.home();
        assertEquals(result,"Das boot, reporting for duty!");
    }
}
