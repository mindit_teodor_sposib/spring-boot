package com.boot;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by MINDIT-PC on 07/06/2017.
 */
@SpringBootApplication
public class App {
    public static void main(String[] args)
    {
        SpringApplication.run(App.class,args);
    }
}
