package com.boot.repository;

import com.boot.model.Shipwreck;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by MINDIT-PC on 07/07/2017.
 */
public interface ShipwreckRepository extends JpaRepository<Shipwreck,Long> {
}
